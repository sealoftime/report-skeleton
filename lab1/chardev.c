#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <linux/string.h>
#include <linux/uaccess.h>
#include <linux/device.h>
#include <linux/kdev_t.h>

MODULE_LICENSE("WTFPL");
MODULE_AUTHOR("Matvei Vdovitsyn");
MODULE_DESCRIPTION("Module that provides a peculiar device, exists for the sole purpose of education.");
MODULE_VERSION("0.1");

dev_t dev;
static struct proc_dir_entry* entry;
static struct class* class;
static struct device* device;

static ssize_t proc_write(struct file *file, const char __user * ubuf, size_t count, loff_t* ppos) 
{
	printk(KERN_DEBUG "Attempt to write proc file");
	return -1;
}

static ssize_t proc_read(struct file *file, char __user * ubuf, size_t count, loff_t* ppos) 
{
	size_t len = strlen(THIS_MODULE->name);
	if (*ppos > 0 || count < len)
	{
		return 0;
	}
	if (copy_to_user(ubuf, THIS_MODULE->name, len) != 0)
	{
		return -EFAULT;
	}
	*ppos = len;
	return len;
}

static struct file_operations proc_fops = {
	.owner = THIS_MODULE,
	.read = proc_read,
	.write = proc_write,
};


static int __init proc_example_init(void)
{
	alloc_chrdev_region(&dev, 0, 1, "var2_Dev");

	class = class_create(THIS_MODULE, THIS_MODULE->name);
	printk(KERN_INFO "%s: class created\n", THIS_MODULE->name);
	device = device_create(class, NULL, dev, NULL, "dev2");
	printk(KERN_INFO "%s: device created\n", THIS_MODULE->name);

	entry = proc_create("var2", 0444, NULL, &proc_fops);
	printk(KERN_INFO "%s: proc file is created\n", THIS_MODULE->name);
	return 0;
}

static void __exit proc_example_exit(void)
{
	proc_remove(entry);
	device_destroy(class, device);
	class_destroy(class);
	unregister_chrdev_region(dev, 1);
}

module_init(proc_example_init);
module_exit(proc_example_exit);

